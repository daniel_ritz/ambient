import { type TextCellProps } from '@am/cells/TextCell.t';

export interface BannerProps extends TextCellProps {
    vibrant?: boolean;
    stacked?: boolean;
}
