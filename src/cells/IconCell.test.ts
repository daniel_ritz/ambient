import { mount } from "@vue/test-utils";
import { describe, expect, test } from "vitest";
import IconCell from "./IconCell.vue";
import {Camera} from 'lucide-vue-next';
import {ApachemavenIcon} from 'vue3-simple-icons';
import { defineComponent } from "vue";

describe("Lucide Icons", () => {

    test("mount Lucide Icon", () => {
        expect(IconCell).toBeTruthy()

        const wrapper = mount(IconCell, {
            props: {
                icon: Camera
            }
        })
        expect(wrapper.html()).toMatchSnapshot()
    })

    test("mount with size", () => {
        expect(IconCell).toBeTruthy()

        const wrapper = mount(IconCell, {
            props: {
                icon: Camera,
                iconSize: 40,
            }
        })
        expect(wrapper.html()).toMatchSnapshot()
    })

})

describe("Simple Icons", () => {

    test("mount Simple Icon", () => {
        expect(IconCell).toBeTruthy()

        const wrapper = mount(IconCell, {
            props: {
                icon: ApachemavenIcon
            }
        })
        expect(wrapper.html()).toMatchSnapshot()
    })

    test("mount with size", () => {
        expect(IconCell).toBeTruthy()

        const wrapper = mount(IconCell, {
            props: {
                icon: ApachemavenIcon,
                size: 40,
            }
        })
        expect(wrapper.html()).toMatchSnapshot()
    })

})