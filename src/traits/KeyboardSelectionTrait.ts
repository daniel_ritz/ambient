import { onMounted, ref, type Ref } from "vue";

export function useKeyboardSelection(el: Ref<HTMLElement | null>, itemCount: Ref<number>, initialFocus: () => number | null, selectItem: (index: number | null, event: PointerEvent | KeyboardEvent) => void) {

    const focussedIndex = ref<number | null>(null)

    function nextItem() {
        const currentIndex = focussedIndex.value
        if (currentIndex == null) {
            focussedIndex.value = itemCount.value > 0 ? 0 : null
            return
        }

        focussedIndex.value =
            currentIndex + 1 < itemCount.value
                ? currentIndex + 1
                : 0
    }

    function prevItem() {
        const currentIndex = focussedIndex.value
        if (currentIndex == null) {
            focussedIndex.value = itemCount.value > 0 ? 0 : null
            return
        }

        focussedIndex.value =
            currentIndex - 1 >= 0
                ? currentIndex - 1
                : itemCount.value - 1
    }

    onMounted(() => {
        el.value?.addEventListener('keyup', (event: KeyboardEvent) => {
            if (event.key == "ArrowUp") {
                prevItem()
            } else if (event.key == "ArrowDown") {
                nextItem()
            } else if (event.key == " ") {
                selectItem(focussedIndex.value, event)
            }
        })

        el.value?.addEventListener('focusin', () => {
            focussedIndex.value = initialFocus()
        })
        el.value?.addEventListener('focusout', () => {
            focussedIndex.value = null
        })
    })

    return {
        focussedIndex, focusItem: (index: number | null) => {
            focussedIndex.value = index
        }
    }

}