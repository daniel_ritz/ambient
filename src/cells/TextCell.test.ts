
import { test, expect, describe } from 'vitest'
import { mount, shallowMount } from '@vue/test-utils'
import TextCell from './TextCell.vue'

describe("TextCell", () => {
    test("mount standalone", async () => {
        expect(TextCell).toBeTruthy()
    
        const wrapper = mount(TextCell, {
            props: {
                text: "Foo",
                tooltip: "Bar",
            },
        })
        expect(wrapper.text()).toBe("Foo")
        expect(wrapper.html()).toMatchSnapshot()
    })
})

