import type { IconCellProps } from '@am/cells/IconCell.t';
import type { TextCellProps } from '@am/cells/TextCell.t';
import { type Action } from '@am/traits/ActionBus';
import type { Component } from 'vue';

export interface MenuItemProps extends TextCellProps, IconCellProps {
    actionData?: any;
    cell?: Component;
    item?: Component;
    submenu?: MenuItemProps[];
    onTap?: Action;
}
