
export interface ScaffoldingProps {
toolbar?: boolean;
header?: 'hero' | false;
padContent?: boolean;
scrollContent?: boolean;
}
