import { inject, provide, ref, type App, type InjectionKey, type Ref } from "vue";

export type Action = string | ((data: any, event: any) => void)
export type ActionBusHandler = (action: string, data: any, event: any) => void
export type ActionBusHook = (action: string, data: any, event: any, bus?: ActionBus) => boolean

const symbol = 'am:actionbus'
let debugMode = false

export function enableDebugMode() {
    debugMode = true
}

class ActionBus {
    constructor(private handler: ActionBusHandler | null) { }
    send(action: Action | undefined, payload: any, event?: any) {
        if (action == undefined) return
        if (typeof action == "string") {
            const actionBus = this.handler
            if (!actionBus) return
            actionBus(action, payload, event)
        } else {
            action(payload, event)
        }
    }
    hook(hook: ActionBusHook) {
        registerActionBus((action, data, event) => {
            if (hook(action, data, event, this)) {
                this.send(action, data, event)
            }
        })
    }
}

export function match(hooks: { [key: string]: { (data: any, event: any, bus?: ActionBus): boolean } }): ActionBusHook {
    return (action, data, event, bus) => {
        if (action in hooks) {
            return hooks[action](data, event, bus)
        }
        return true
    }
}

export function useActionBus() {
    return new ActionBus(inject(symbol, null) as ActionBusHandler | null)
}

export function registerActionBus(handler: ActionBusHandler) {
    provide(symbol, handler)
}

export function registerActionBusTerminator(app: App, hooks: ActionBusHook[]) {
    let handler: ActionBusHandler = (action, data, event) => {
        for (const hook of hooks.reverse()) {
            if (!hook(action, data, event, undefined)) return
        }

        if (debugMode) {
            console.warn("Unhandled Action", action, data)
        }
    }
    app.provide(symbol, handler)
}

export type Trigger = number
export function trigger(pulse: Ref<number>) {
    pulse.value++
}
export function newTrigger(): Ref<Trigger> {
    return ref(0)
}