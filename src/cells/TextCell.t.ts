import { Formatter } from "@am/traits/Formatter";

export interface TextCellProps {
    text?: string,
    textKey?: string,
    tooltip?: string,
    tooltipKey?: string,
    formatter?: Formatter,
    kind?: 'heading' | 'h2' | 'h3' | 'title' | 'secondary' | 'strong',
    lead?: boolean,
}