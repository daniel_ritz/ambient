import type { TextCellProps } from '@am/cells/TextCell.t';
import type { IconCellProps } from '@am/cells/IconCell.t';
import { type Action } from '@am/traits/ActionBus';
import type { AccessibilityProps } from '@am/traits/Accessibility.t';


export interface ButtonProps
extends TextCellProps, IconCellProps, AccessibilityProps {
disabled?: boolean;

actionData?: any;
onTap?: Action;
onContext?: Action;
}
