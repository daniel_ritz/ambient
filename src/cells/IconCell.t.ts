import type { Icon as LucideIcon } from 'lucide-vue-next';

export type Icon = LucideIcon | Object;

export interface IconCellProps {
    icon?: any;
    iconSize?: number;
}
