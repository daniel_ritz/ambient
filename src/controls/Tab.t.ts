import { type ButtonProps } from './BaseButton.t';


export interface TabProps extends ButtonProps {
active?: boolean;
closeable?: boolean;
}
