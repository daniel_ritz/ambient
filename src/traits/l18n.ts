import { inject, computed, type Ref, ref, type App } from 'vue';
import type { AmbientAppInstallOptions } from './AmbientApp';

const symbol = 'am::l18n'
let debugMode = false

export function enableDebugMode() {
    debugMode = true
}

export function getText(key?: Ref<string|undefined>, fallback?: Ref<string|undefined>): Ref<string|undefined> {
    const l18n = inject(symbol) as Ref<any>
    return computed(() => {
        if (key != undefined && key.value != undefined && key.value in l18n.value) {
            return l18n.value[key.value]
        } else {
            if (debugMode) return "***"
            return fallback?.value
        }  
    })   
}

export function installL18n(texts: Ref<{[key: string]: string}>): AmbientAppInstallOptions {
    return {
        provide: {
            [symbol]: texts
        }
    }
}