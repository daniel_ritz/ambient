import type { ButtonProps } from '@am/controls/BaseButton.t';


export interface ToolbarAction extends ButtonProps {
    actionType: 'button' | 'toggle' | 'menu' | 'selection' | 'pulldown';
    active?: boolean;
}
