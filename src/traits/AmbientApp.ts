import { createApp, ref, type App, type Component, type Ref } from "vue";
import { registerActionBusTerminator, type ActionBusHook } from "./ActionBus";
import { installL18n } from "./l18n";
import { installNavigationGraph, type DerivePathHandler, type ResolveViewHandler, type RestoreFromPathHandler } from "./NavigationGraph";

export type AmbientAppOptions = {
    rootProps?: any,
    l18n?: Ref<{[key: string]: string}>,
}

export type AmbientAppInstallOptions = {
    provide?: {[key: string]: any},
    actionBusHooks?: ActionBusHook[],
    install?: {(): void}
}

export class AmbientAppBuilder {
    private traits: AmbientAppInstallOptions[] = []
    private rootElement: Component
    private rootProps: any
    private resolveViewHandler?: ResolveViewHandler
    private derivePathHandler?: DerivePathHandler
    private restoreFromPathHandler?: RestoreFromPathHandler
    private l18nTexts = ref<{[key: string]: string}>({})

    constructor(rootElement: Component) {
        this.rootElement = rootElement
    }

    setProps(props: any) {
        this.rootProps = props
    }

    addTrait(trait: AmbientAppInstallOptions) {
        this.traits.push(trait)
    }

    provideL18n(texts: Ref<{[key: string]: string}>) {
        this.l18nTexts = texts
    }

    resolveView(handler: ResolveViewHandler) {
        this.resolveViewHandler = handler
    }

    derivePath(handler: DerivePathHandler) {
        this.derivePathHandler = handler
    }

    restoreFromPath(handler: RestoreFromPathHandler) {
        this.restoreFromPathHandler = handler
    }

    build(): App<Element> {
        this.registerDefaultTraits()

        const app = createApp(this.rootElement, this.rootProps)
        this.registerProvides(app)
        const actionBusHooks = this.collectActionBusHooks()
        registerActionBusTerminator(app, actionBusHooks)
        this.runInstallHooks()
        return app
    }

    private registerDefaultTraits() {
        this.addTrait(installL18n(this.l18nTexts))
        this.addTrait(installNavigationGraph({
            resolveView: this.resolveViewHandler,
            derivePath: this.derivePathHandler,
            restoreFromPath: this.restoreFromPathHandler,
        }))
    }

    private registerProvides(app: App) {
        this.traits
            .filter(t => t.provide != undefined)
            .map(t => t.provide!)
            .forEach(provides => {
                Object.keys(provides).forEach(key => {
                    app.provide(key, provides[key])
                })
            })
    }

    private collectActionBusHooks() {
        return this.traits
            .filter(t => t.actionBusHooks != undefined)
            .map(t => t.actionBusHooks!)
            .flat()
    }

    private runInstallHooks() {
        this.traits
            .filter(t => t.install != undefined)
            .forEach(t => t.install!())
    }

}