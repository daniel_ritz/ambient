export type StackHint = {
    hint: 'stack', 
    scaffolding: string,
}
export type DialogHint = {
    hint: 'dialog',
    scaffolding?: string,
    noCancel?: boolean,
    wide?: boolean,
}
export type PlacesHint = {
    hint: 'places',
    emptyView: string,
}

export type NavigationHints = StackHint | DialogHint | PlacesHint
export const NavigationAction = {
    Back: "navigation::back",
    Push: "navigation::push",
    Swap: "navigation::swap",
    OpenContext: "navigation::open-context",
    DropContext: "navigation::drop-context",
}