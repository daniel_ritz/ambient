let counter = 0
export function newId(tag?: string): string {
    const num = ++counter
    return `${tag ?? 'id'}${num}`
}