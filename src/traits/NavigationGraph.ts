import { inject, provide, ref, type Component, type Ref } from "vue"
import { match } from "./ActionBus"
import type { AmbientAppInstallOptions } from "./AmbientApp"
import { NavigationAction, type NavigationHints } from "./Navigation"

export interface NavigationFrame {
    hints?: NavigationHints[],
    view: string,
    data?: any,
}
export interface NavigationContext {
    hints: NavigationHints[],
    frames: NavigationFrame[],
}
export type NavigationGraph = NavigationContext[]
export type ResolveViewHandler = {(view: string): Component|undefined}
export type DerivePathHandler = {(graph: NavigationGraph): string|undefined}
export type RestoreFromPathHandler = {(): void}
export interface NavigationGraphHelper {
    graph: Ref<NavigationGraph>,
    provideSubGraph: {(graph: Ref<NavigationGraph>): void},
    resolveView: ResolveViewHandler,
}

type HistoryState = {
    id: number,
    obsolete?: boolean,
}

const symbol = "am::ng"
const graph = ref<NavigationGraph>([])
const states = <{[key: number]: NavigationGraph}>{}
let resolveViewHandler: ResolveViewHandler = (_) => {console.error('Cannot resolve', _); return undefined}
let derivePathHandler: DerivePathHandler = (_) => undefined
let restoreFromPathHandler: RestoreFromPathHandler = () => {}

function clone(obj: any) {
    // Deep clone
    return JSON.parse(JSON.stringify(obj))
}

function saveState() {
    const id = Date.now()
    states[id] = clone(graph.value)
    return id
}

export function pushFrame(frame: NavigationFrame) {
    // Push a new frame to the topmost context
    if (graph.value.length == 0) console.error("Error pushing NavigationFrame: No NavigationContext")
    const context = graph.value[graph.value.length-1]
    context.frames.push(frame)
    history.pushState({id: saveState()}, "", derivePathHandler(graph.value))
}

export function swapFrame(frame: NavigationFrame) {
    // Swap the topmost frame
    if (graph.value.length == 0) console.error("Error pushing NavigationFrame: No NavigationContext")
    const context = graph.value[graph.value.length-1]
    context.frames.splice(context.frames.length-1, 1, frame)
    history.replaceState({id: saveState()}, "", derivePathHandler(graph.value))
}

export function pushContext(frame: NavigationFrame, contextHints?: NavigationHints[]) {
    // Push a new context with an initial frame
    graph.value.push({
        hints: contextHints ?? [],
        frames: [frame]
    })
    history.pushState({id: saveState()}, "", derivePathHandler(graph.value))
}

export function dropContext() {
    // Drop the topmost context
    if (graph.value.length == 0) console.error("Error dropping NavigationContext: No NavigationContext")
    const currentContext = graph.value[graph.value.length-1]
    history.go(-1 * currentContext.frames.length)
}

export function dropFrame() {
    // Drop the topmost frame
    history.back()
}

function restoreState() {
    const state = <HistoryState>history.state
    if (!('id' in state) || typeof state.id != 'number') {
        console.log('invalid state', state)
        return // TODO restore initial state
    }
    if (state.obsolete) {
        history.back() // state is obsolete => skip this state
        return 
    }
    if (!(state.id in states)) {
        console.log('id not found', state.id)
        restoreFromPathHandler()
        return
    }
    
    graph.value = clone(states[state.id])
    // history.replaceState({id: state.id, obsolete: true}, "")
    // history.pushState({id: state.id}, "")
}

export function installNavigationGraph(options: {
    resolveView?: ResolveViewHandler,
    derivePath?: DerivePathHandler,
    restoreFromPath?: RestoreFromPathHandler,
}): AmbientAppInstallOptions {
    return {
        provide: {
            [symbol]: graph,
        },
        install() {
            if (options.resolveView) resolveViewHandler = options.resolveView
            if (options.derivePath) derivePathHandler = options.derivePath
            if (options.restoreFromPath) restoreFromPathHandler = options.restoreFromPath
            window.addEventListener('popstate', restoreState)
            restoreFromPathHandler()
        },
        actionBusHooks: [
            match({
                [NavigationAction.Push]: (frame: NavigationFrame) => {
                    pushFrame(frame)
                    return false
                },
                [NavigationAction.Swap]: (frame: NavigationFrame) => {
                    swapFrame(frame)
                    return false
                },
                [NavigationAction.OpenContext]: (data: {frame: NavigationFrame, hints?: NavigationHints[]}) => {
                    pushContext(data.frame, data.hints)
                    return false
                },
                [NavigationAction.DropContext]: () => {
                    dropContext()
                    return false
                },
                [NavigationAction.Back]: () => {
                    dropFrame()
                    return false
                }
            })
        ]
    }
}

export function useNavigationGraph(): NavigationGraphHelper {
    const graph = inject<Ref<NavigationGraph>>(symbol, ref([]))
    return {
        graph,
        provideSubGraph: (graph: Ref<NavigationGraph>) => {
            provide(symbol, graph)
        },
        resolveView: resolveViewHandler,
    }
}
