import { ref } from "vue"

export type DragDropProps = {
    allowDrag?: boolean,
    allowDrop?: boolean,
    onDrag?: (event: DragEvent, actionData: any) => void,
    onDragResult?: (event: DragEvent, actionData: any) => void,
    onNegotiateDrop?: (event: DragEvent) => boolean,
    onDrop?: (event: DragEvent) => void,
}

export function useDragDrop(props: DragDropProps, dropPreview: (event: DragEvent) => void, dropCleanup: () => void) {

    let dragEnterDepth = 0

    function dragStart(event: DragEvent, actionData: any) {
        if (!props.onDrag) return
        props.onDrag(event, actionData)
    }

    function dragEnter() {
        dragEnterDepth += 1
        if (dragEnterDepth < 1) dragEnterDepth = 1
    }

    function dragOver(event: DragEvent) {
        if (!props.onNegotiateDrop) return
        if (!props.allowDrop) {
            dropCleanup()
            return
        }
        if (props.onNegotiateDrop(event)) {
            event.preventDefault()
            dropPreview(event)
        } else {
            dropCleanup()
        }
    }

    function dragLeave() {
        dragEnterDepth -= 1
        // Prevent jitter when entering child items
        if (dragEnterDepth <= 0) {
            dropCleanup()
        }
    }

    function dragEnd(event: DragEvent, actionData: any) {
        dragEnterDepth = 0
        dropCleanup()
        if (!props.onDragResult) return
        props.onDragResult(event, actionData)
    }

    function drop(event: DragEvent) {
        dragEnterDepth = 0
        dropCleanup()
        if (!props.onDrop) return
        props.onDrop(event)
        event.preventDefault()
    }


    return {
        dragStart,
        dragOver,
        dragEnter,
        dragLeave,
        dragEnd,
        drop,
    }

}