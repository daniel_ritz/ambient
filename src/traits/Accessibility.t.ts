export interface AccessibilityProps {
    alt?: string,
    altKey?: string,
}