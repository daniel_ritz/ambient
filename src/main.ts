import { createApp, ref } from 'vue'
import App from './App.vue'
import TextCellVue from './cells/TextCell.vue'
import Nav1Vue from './example/Nav1.vue'
import Nav2Vue from './example/Nav2.vue'
import Nav3Vue from './example/Nav3.vue'

import './theme/theme.scss'
import { AmbientAppBuilder } from './traits/AmbientApp'
import { installL18n } from './traits/l18n'
import { pushContext, pushFrame } from './traits/NavigationGraph'

const builder = new AmbientAppBuilder(App)
builder.provideL18n(ref({
    hello: "Hello",
    save: "Save",
}))
builder.resolveView((view) => {
    switch(view){
        case 'nav1':
            return Nav1Vue
        case 'nav2':
            return Nav2Vue
        case 'nav3':
            return Nav3Vue
    }
    return TextCellVue
})
builder.derivePath((graph) => {
    if (graph.length == 1) {
        if (graph[0].frames.length == 1) return '#/'
        else return '#/nav2'
    } else {
        return `#/nav3/${graph[1].frames.length}`
    }
})
builder.restoreFromPath(() => {
    const path = location.hash
    console.log('path is', path)
    if (path == '#/nav2') {
        pushContext({
            hints: [],
            view: 'nav1',
            data: {text: "Foo"}
        }, [])
        pushFrame({
            hints: [],
            view: 'nav2',
            data: {text: "Bar"}
        })
    } else if (path.substring(0, 7) == '#/nav3/') {
        const num = parseInt(path.substring(path.lastIndexOf('/')+1))
        console.log('num is', num)
        pushContext({
            hints: [],
            view: 'nav1',
            data: {text: "Foo"}
        }, [])
        pushFrame({
            hints: [],
            view: 'nav2',
            data: {text: "Bar"}
        })
        pushContext({
            hints: [],
            view: 'nav3',
            data: {num: 1}
        }, [])
        for (let i = 2; i <= num; ++i) {
            pushFrame({
                hints: [],
                view: 'nav3',
                data: {num: i}
            })
        }
    } else {
        pushContext({
            hints: [],
            view: 'nav1',
            data: {text: "Foo"}
        }, [])
    }
})

const app = builder.build()

app.mount('#app')
