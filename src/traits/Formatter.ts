import { unref, type Ref } from "vue"

export type Formatter = {(text: string|undefined): string|undefined}

export function numberFormatter(decimalDigits?: number, leadingDigits?: number, language?: string): Formatter {
    const formatter = new Intl.NumberFormat(language, {
        minimumIntegerDigits: leadingDigits,
        maximumFractionDigits: decimalDigits,
    })
    return (text) => {
        if (text == undefined) return undefined
        const num = parseFloat(text)
        return formatter.format(num)
    }
}

type ReplaceValue = string|number|{[key: string]: string|number}
export function replace(values: Ref<ReplaceValue>|ReplaceValue): Formatter {
    return (text) => {
        if (text == undefined) return undefined
        const v = unref(values)
        if (typeof v == 'string') {
            return text.replaceAll('{$}', v)
        } else if (typeof v == 'number') {
            return text.replaceAll('{$}', `${v}`)
        } else {
            let t = text
            for (const key of Object.keys(v)) {
                t = t.replaceAll(`{$${key}}`, `${v[key]}`)
            }
            return t
        }
    }    
}